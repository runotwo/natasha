from django.db import models


class Question(models.Model):
    class TYPES:
        buttons = 1
        text = 2

    points = models.IntegerField()
    name = models.CharField(max_length=512)
    description = models.TextField()
    text = models.TextField()
    ans_type = models.IntegerField()
    is_first = models.BooleanField()
    next_question = models.ForeignKey('Question', on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'Вопросы'


class Answer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    text = models.TextField()
    correct = models.BooleanField()

    class Meta:
        verbose_name_plural = 'Ответы'


class AnswersLog(models.Model):
    curator = models.ForeignKey('curators.Profile', on_delete=models.SET_NULL, null=True)
    question = models.ForeignKey('Question', on_delete=models.SET_NULL, null=True)
    answer_text = models.TextField()
    correct = models.BooleanField()
    created_at = models.DateTimeField()

    class Meta:
        verbose_name_plural = 'Ответы на вопросы'
