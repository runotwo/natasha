from django.apps import AppConfig


class SeancesConfig(AppConfig):
    name = 'seances'
    verbose_name = 'События'
