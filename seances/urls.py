from django.urls import path

from .views import (
    EndedEventsView, EventsView, EventView, ReservedView, RoomsView, ReserveView, ReviewView,
    CancelView, CuratorsView, SearchView, AdditionalCuratorsView, ProfileView, AchievementsView, StudentsView,
    AttendanceView, SendAttendanceAsk, SendFeedbackAsk, SetAttendance, FeedbackView, NewsView, SendNewsView,
    AchievementAskView, RatingView, Logout, AttendanceMessage, RatingsView, CuratorReviewView, CEventView,
    EventReviewView, MaterialsView, MaterialView, LastEventView
)

urlpatterns = [
    path('events/', CEventView.as_view()),
    path('last_event/', LastEventView.as_view()),
    path('events/<int:event_id>/', EventReviewView.as_view()),
    path('seances/', EventsView.as_view()),
    path('ended_seances/', EndedEventsView.as_view()),
    path('seances/<int:seance_id>/', EventView.as_view()),
    path('seances/<int:room_id>/reservations/', ReservedView.as_view()),
    path('rooms/', RoomsView.as_view()),
    path('seances/<int:seance_id>/reserve/', ReserveView.as_view()),
    path('seances/<int:seance_id>/cancel/', CancelView.as_view()),
    path('seances/<int:seance_id>/review/', ReviewView.as_view()),
    path('reviews/', ReviewView.as_view()),
    path('curators', CuratorsView.as_view()),
    path('curators/<int:curator_id>/review/', CuratorReviewView.as_view()),
    path('search_curators', SearchView.as_view()),
    path('additional_curators', AdditionalCuratorsView.as_view()),
    path('profile', ProfileView.as_view()),
    path('achievements', AchievementsView.as_view()),

    path('seance/<int:seance_id>/attendance/', AttendanceView.as_view()),
    path('seance/<int:seance_id>/ask_attendance/', SendAttendanceAsk.as_view()),
    path('seance/<int:seance_id>/attendance_ask/<int:news_id>/', AttendanceMessage.as_view()),
    path('seance/<int:seance_id>/attendance/<int:atd_id>/', SetAttendance.as_view()),

    path('seance/<int:seance_id>/feedback/', FeedbackView.as_view()),
    path('seance/<int:seance_id>/feedback_ask/', SendFeedbackAsk.as_view()),

    path('news/', NewsView.as_view()),
    path('news/<int:news_id>/', SendNewsView.as_view()),

    path('students/', StudentsView.as_view()),

    path('achievement_asks', AchievementAskView.as_view()),
    path('rating', RatingView.as_view()),
    path('ratings', RatingsView.as_view()),
    path('materials/', MaterialsView.as_view()),
    path('materials/<int:material_id>/', MaterialView.as_view()),

    path('logout', Logout.as_view()),
]
