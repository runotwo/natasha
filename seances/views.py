import datetime
import json

from django.contrib.auth import logout
from django.db.models import Q
from django.db.models import Sum, Min
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.utils.timezone import now
from django.views import View

# from curators.bots.sender import Sender
from curators.models import Profile
from polls.models import News
from rating.models import Achievement, AchievementAsk, CuratorReview
from seances.models import Seance, Helper, Review, Classroom, Attendance, Feedback, Event, Material


# from . import helpers


class EndedEventsView(View):

    def get(self, request):
        seances = Seance.objects.filter(time_end__lte=now() + datetime.timedelta(hours=3),
                                        group=request.user.profile.students_group)
        result = [{
            'id': s.id,
            'event': {
                'id': s.event.id,
                'type': s.event.type,
                'title': s.event.title,
                'goal': s.event.goal,
                'date_start': s.event.date_start.strftime('%d/%m/%Y'),
                'date_end': s.event.date_end.strftime('%d/%m/%Y'),
                'photo': None if s.event.photo.name == '' else s.event.photo.url
            } if s.event else None,
            'time_start': s.time_start if s.time_start is None else s.time_start.strftime('%m/%d/%YT%H:%M'),
            'time_end': s.time_end if s.time_end is None else s.time_end.strftime('%m/%d/%YT%H:%M'),
            'room': {
                'id': s.room.id,
                'title': s.room.title,
            } if s.room else None,
            'has_review': Review.objects.filter(seance=s, curator=request.user.profile).exists()
        } for s in seances]
        return JsonResponse(result, safe=False)


class EventsView(View):

    def get(self, request):
        seances = Seance.objects.filter(Q(time_end__gt=now() + datetime.timedelta(hours=3)) | Q(time_end__isnull=True),
                                        group=request.user.profile.students_group)
        result = [{
            'id': s.id,
            'event': {
                'id': s.event.id,
                'type': s.event.type,
                'title': s.event.title,
                'goal': s.event.goal,
                'date_start': s.event.date_start.strftime('%d/%m/%Y'),
                'date_end': s.event.date_end.strftime('%d/%m/%Y'),
                'photo': None if s.event.photo.name == '' else s.event.photo.url
            } if s.event else None,
            'time_start': s.time_start if s.time_start is None else s.time_start.strftime('%m/%d/%YT%H:%M'),
            'time_end': s.time_end if s.time_end is None else s.time_end.strftime('%m/%d/%YT%H:%M'),
            'room': {
                'id': s.room.id,
                'title': s.room.title,
            } if s.room else None,
        } for s in seances]
        return JsonResponse(result, safe=False)


class LastEventView(View):

    def get(self, request):
        s = Seance.objects.filter(group=request.user.profile.students_group).last()
        result = {
            'id': s.id,
            'event': {
                'id': s.event.id,
                'type': s.event.type,
                'title': s.event.title,
                'goal': s.event.goal,
                'date_start': s.event.date_start.strftime('%d/%m/%Y'),
                'date_end': s.event.date_end.strftime('%d/%m/%Y'),
                'photo': None if s.event.photo.name == '' else s.event.photo.url
            } if s.event else None,
            'time_start': s.time_start if s.time_start is None else s.time_start.strftime('%m/%d/%YT%H:%M'),
            'time_end': s.time_end if s.time_end is None else s.time_end.strftime('%m/%d/%YT%H:%M'),
            'room': {
                'id': s.room.id,
                'title': s.room.title,
            } if s.room else None,
        }
        return JsonResponse(result, safe=False)


class EventView(View):

    def get(self, request, seance_id):
        s = get_object_or_404(Seance, pk=seance_id)
        helpers = Helper.objects.filter(curator_hour=s.event)
        result = {
            'id': s.id,
            'event': {
                'id': s.event.id,
                'type': s.event.type,
                'title': s.event.title,
                'goal': s.event.goal,
                'date_start': str(s.event.date_start),
                'date_end': str(s.event.date_end),
                'photo': None if s.event.photo.name == '' else s.event.photo.url
            } if s.event else None,
            'time_start': str(s.time_start),
            'time_end': str(s.time_end),
            'room': {
                'id': s.room.id,
                'title': s.room.title,
            } if s.room else None,
            'helpers': [
                {
                    'title': h.title,
                    'text': h.text,
                    'attachment': None if h.attachments.name == '' else h.attachments.url
                } for h in helpers
            ],
            'has_review': Review.objects.filter(seance=s, curator=request.user.profile).exists(),
        }
        return JsonResponse(result, safe=False)


class ReservedView(View):
    def get(self, request, room_id):
        data = json.loads(request.body)
        date = data['date']
        fday = datetime.datetime.strptime(date, '%m/%d/%Y')
        sday = fday + datetime.timedelta(days=1)
        room = get_object_or_404(Classroom, pk=room_id)
        events = Seance.objects.filter(room=room, time_start__gte=fday, time_start__lt=sday).order_by('time_start')
        result = [
            {
                'start': e.time_start.strftime('%H:%M'),
                'end': e.time_end.strftime('%H:%M'),
            } for e in events
        ]
        return JsonResponse(result, safe=False)


class RoomsView(View):
    def get(self, request):
        start = datetime.datetime.strptime(request.GET['time_start'], '%m/%d/%YT%H:%M')
        end = datetime.datetime.strptime(request.GET['time_end'], '%m/%d/%YT%H:%M')
        query = Q(time_end__gt=start) & Q(time_start__lt=end) & Q(room__isnull=False)
        ex_rooms = Seance.objects.filter(query).values_list('room', flat=True)
        rooms = Classroom.objects.all().exclude(id__in=ex_rooms)
        result = [
            {
                'id': r.id,
                'name': r.title,
            } for r in rooms
        ]
        return JsonResponse(result, safe=False)


class ReserveView(View):
    def post(self, request, seance_id):
        data = json.loads(request.body.decode())
        start = datetime.datetime.strptime(data['time_start'], '%m/%d/%YT%H:%M')
        end = datetime.datetime.strptime(data['time_end'], '%m/%d/%YT%H:%M')
        room_id = data.get('room_id')
        if room_id:
            room_id = int(room_id)
        else:
            room_id = None
        seance = get_object_or_404(Seance, pk=seance_id)
        seance.room_id = room_id
        seance.time_start = start
        seance.time_end = end
        seance.save()
        return JsonResponse({'status': 'OK'}, safe=False)


class CancelView(View):
    def delete(self, request, seance_id):
        seance = get_object_or_404(Seance, pk=seance_id)
        seance.room_id = None
        seance.time_start = None
        seance.time_end = None
        seance.save()
        return JsonResponse({'status': 'OK'}, safe=False)


class ReviewView(View):
    @staticmethod
    def check_achievements(event_id, profile):
        if event_id == 7:
            profile.achievements.add(Achievement.objects.get(id=6))
        if event_id == 12:
            profile.achievements.add(Achievement.objects.get(id=7))
        if event_id == 13:
            profile.achievements.add(Achievement.objects.get(id=8))
        if event_id == 14:
            profile.achievements.add(Achievement.objects.get(id=9))
        if event_id == 16:
            profile.achievements.add(Achievement.objects.get(id=10))
        if Review.objects.filter(curator=profile).count() == 5:
            profile.achievements.add(Achievement.objects.get(id=11))

    def post(self, request, seance_id):
        data = json.loads(request.body.decode())
        seance = get_object_or_404(Seance, pk=seance_id)
        Review.objects.create(how=data['how'], problems=data['problems'], what=data['what'], seance=seance,
                              curator=request.user.profile)
        self.check_achievements(seance.event.id, request.user.profile)
        return JsonResponse({'status': 'OK'}, safe=False)


class CuratorsView(View):
    def get(self, request):
        faculty = request.GET['faculty']
        main = Profile.objects.filter(role='main_curator', faculty__name=faculty)
        curators = Profile.objects.filter(students_group__faculty__name=faculty,
                                          role__in=['main_curator', 'curator']).exclude(id__in=[x.id for x in main]) \
            .order_by('students_group__number')
        result = {
            'main_curators': [
                {
                    'id': c.id,
                    'surname': c.surname,
                    'first_name': c.first_name,
                    'parent_name': '' if c.parent_name is None else c.parent_name,
                    'vk': f'https://vk.com/id{c.vk_id}',
                    'photo': '' if not c.vk_photo else c.vk_photo.url,
                    'group': '' if c.students_group is None else c.students_group.number,
                    'role': c.get_verb_role(),
                } for c in main
            ],
            'curators': [
                {
                    'id': c.id,
                    'surname': c.surname,
                    'first_name': c.first_name,
                    'parent_name': '' if c.parent_name is None else c.parent_name,
                    'vk': f'https://vk.com/id{c.vk_id}',
                    'photo': '' if not c.vk_photo else c.vk_photo.url,
                    'group': '' if c.students_group is None else c.students_group.number,
                    'role': c.get_verb_role(),
                } for c in curators
            ]
        }
        return JsonResponse(result, safe=False)


class SearchView(View):
    def get(self, request):
        query = request.GET['query']
        f = Q(first_name__icontains=query) | Q(surname__icontains=query) | Q(students_group__number__icontains=query) | \
            Q(first_name__icontains=query.capitalize()) | Q(surname__icontains=query.capitalize()) | \
            Q(students_group__number__icontains=query.upper())
        curators = Profile.objects.filter(role__in=['main_curator', 'curator']).filter(f)
        result = {
            'curators': [
                {
                    'surname': c.surname,
                    'first_name': c.first_name,
                    'parent_name': '' if c.parent_name is None else c.parent_name,
                    'vk': f'https://vk.com/id{c.vk_id}',
                    'photo': '' if not c.vk_photo else c.vk_photo.url,
                    'group': '' if c.students_group is None else c.students_group.number,
                    'role': c.get_verb_role(),
                } for c in curators
            ]
        }
        return JsonResponse(result, safe=False)


class AdditionalCuratorsView(View):
    def get(self, request):
        main = Profile.objects.filter(role='main_curator', faculty=request.user.profile.students_group.faculty)
        partners = Profile.objects.filter(students_group=request.user.profile.students_group).exclude(
            id=request.user.profile.id)
        result = {
            'curators': [
                *[{
                    'surname': partner.surname,
                    'first_name': partner.first_name,
                    'parent_name': '' if partner.parent_name is None else partner.parent_name,
                    'vk': f'https://vk.com/id{partner.vk_id}',
                    'photo': '' if not partner.vk_photo else partner.vk_photo.url,
                    'group': '' if partner.students_group is None else partner.students_group.number,
                    'role': 'Напарник',
                } for partner in partners],
                *[
                    {
                        'surname': c.surname,
                        'first_name': c.first_name,
                        'parent_name': '' if c.parent_name is None else c.parent_name,
                        'vk': f'https://vk.com/id{c.vk_id}',
                        'photo': '' if not c.vk_photo else c.vk_photo.url,
                        'group': '' if c.students_group is None else c.students_group.number,
                        'role': 'Старший куратор',
                    } for c in main
                ],
                {
                    'surname': 'Дежиков',
                    'first_name': 'Дмитрий',
                    'parent_name': 'Юрьевич',
                    'vk': 'https://vk.com/dezhik_d',
                    'photo': 'http://curator.sut.ru/media/139760242.jpg',
                    'group': '',
                    'role': 'Руководитель Института кураторов',
                },
            ]
        }
        return JsonResponse(result, safe=False)


class ProfileView(View):
    def get(self, request):
        profile = request.user.profile
        result = {
            'surname': profile.surname,
            'first_name': profile.first_name,
            'parent_name': '' if profile.parent_name is None else profile.parent_name,
            'phone': profile.phone,
            'birth_date': profile.birth_date,
            'vk': f'https://vk.com/id{profile.vk_id}',
            'photo': '' if not profile.vk_photo else profile.vk_photo.url,
            'group': '' if profile.group is None else profile.group.number,
            'faculty': '' if profile.group is None else profile.group.faculty.name,
            'faculty_full': '' if profile.group is None else profile.group.faculty.full_name,
            'supervised_faculty': '' if profile.faculty is None else profile.faculty.name,
            'supervised_faculty_full': '' if profile.faculty is None else profile.faculty.full_name,
            'students_group': '' if profile.students_group is None else profile.students_group.number,
            'students_faculty': '' if profile.students_group is None else profile.students_group.faculty.name,
            'students_faculty_full': '' if profile.students_group is None else profile.students_group.faculty.full_name,
            'role': profile.get_verb_role(),
            'achievements': [
                {
                    'name': a.name,
                    'description': a.description,
                    'picture': '' if not a.picture else a.picture.url,
                    'points': a.points,
                } for a in profile.achievements.all()
            ]
        }
        return JsonResponse(result, safe=False)


class AchievementsView(View):
    @staticmethod
    def ord(x):
        if x['status'] == 'closed':
            return 0
        if x['status'] == 'await':
            return 1
        return 2

    def get(self, request):
        ach = Achievement.objects.all()
        result = [
            {
                'name': a.name,
                'description': a.description,
                'picture': '' if not a.picture else a.picture.url,
                'points': a.points,
                'status': a.get_status(request.user.profile),
            } for a in ach
        ]
        return JsonResponse(sorted(result, key=self.ord), safe=False)


class AttendanceView(View):
    def get(self, request, seance_id):
        atd = Attendance.objects.filter(seance_id=seance_id)
        result = [
            {
                'id': a.id,
                'student': {
                    'surname': a.profile.surname,
                    'first_name': a.profile.first_name,
                    'parent_name': a.profile.parent_name,
                },
                'would': a.would,
                'was': a.here,
            } for a in atd
        ]
        return JsonResponse(result, safe=False)


class SendAttendanceAsk(View):
    def get(self, request, seance_id):
        from curators.bots.vk_bot import VkBot
        atd = Attendance.objects.filter(seance_id=seance_id)
        group = atd[0].seance.group
        bot = VkBot(group)
        text = '''{}
Время начала: {}
Место: {}

Отправь 1, если обязательно придешь
Отправь 2, если прийти не получится'''.format(atd[0].seance.event.title,
                                              atd[0].seance.time_start.strftime('%d.%m.%Y %H:%M'),
                                              'Другое' if atd[0].seance.room is None else atd[
                                                  0].seance.room.title)
        news_id = request.GET.get('news_id')
        if news_id:
            news_id = int(news_id)
            news = News.objects.filter(id=news_id).last()
            if news:
                text = f'{news.text}\n\n' + text
                news.sended_at = now() + datetime.timedelta(hours=3)
                news.save()
        for a in atd:
            bot.send_response_message({'user_id': a.profile.vk_id, 'text': text})
            a.profile.status = Profile.Status.ATTENDANCE_ASKED
            a.profile.asked_seance = a.seance
            a.profile.save()
        for c in group.curator_profiles.all():
            try:
                bot.send_response_message({'user_id': c.profile.vk_id, 'text': '''Ты забронировал кураторский час

{}
Время начала: {}
Место: {}'''.format(atd[0].seance.event.title,
                    atd[0].seance.time_start.strftime('%d.%m.%Y %H:%M'),
                    'Другое' if atd[0].seance.room is None else atd[0].seance.room.title)})
            except:
                pass
        return JsonResponse({'status': 'OK'}, safe=False)


class AttendanceMessage(View):
    def get(self, request, seance_id, news_id):
        news = get_object_or_404(News, pk=news_id)
        seance = get_object_or_404(Seance, pk=seance_id)
        text = '''{}

{}
Время начала: {}
Место: {}

Отправь 1, если обязательно придешь
Отправь 2, если прийти не получится'''.format(news.text,
                                              seance.event.title,
                                              seance.time_start.strftime('%d.%m.%Y %H:%M'),
                                              'Другое' if seance.room is None else seance.room.title)
        return JsonResponse({'text': text}, safe=False)


class SendFeedbackAsk(View):
    def get(self, request, seance_id):
        from curators.bots.vk_bot import VkBot
        seance = get_object_or_404(Seance, pk=seance_id)
        bot = VkBot(seance.group)
        students = [x.profile for x in seance.attendance_set.filter(here=True)]
        for student in students:
            bot.send_response_message({'user_id': student.vk_id,
                                       'text': f'Вот и прошел кураторский час! Пожалуйста, напиши в следующем сообщении небольшой отзыв. Отметь, что тебе понравилось, а что нет'})
            student.status = student.Status.FEEDBACK_ASKED
            student.asked_seance = seance
            student.save()
        return JsonResponse({'status': 'OK'}, safe=False)


class SetAttendance(View):
    def post(self, request, seance_id, atd_id):
        atd = get_object_or_404(Attendance, pk=atd_id)
        data = json.loads(request.body)
        would = data.get('would', False)
        was = data.get('was', False)
        atd.would = would
        atd.here = was
        atd.save()
        return JsonResponse({'status': 'OK'}, safe=False)


class FeedbackView(View):
    def get(self, request, seance_id):
        f = Feedback.objects.filter(seance_id=seance_id)
        result = [
            {
                'id': a.id,
                'student': {
                    'surname': a.student.surname,
                    'first_name': a.student.first_name,
                    'parent_name': '' if a.student.parent_name is None else a.student.parent_name,
                },
                'text': a.text,
                'points': a.points,
            } for a in f
        ]
        return JsonResponse(result, safe=False)


class NewsView(View):
    def get(self, request):
        f = News.objects.filter(group=request.user.profile.students_group).order_by('-id')
        result = [
            {
                'id': a.id,
                'title': a.title,
                'text': a.text,
                'created_at': "" if a.created_at is None else (a.created_at + datetime.timedelta(hours=3)).strftime(
                    '%m/%d/%YT%H:%M'),
                'sended_at': "" if a.sended_at is None else (a.sended_at + datetime.timedelta(hours=3)).strftime(
                    '%m/%d/%YT%H:%M'),
            } for a in f
        ]
        return JsonResponse(result, safe=False)

    def post(self, request):
        data = json.loads(request.body)
        title = data.get('title', "")
        text = data.get('text', "")
        a = News.objects.create(title=title, text=text, created_by=request.user.profile,
                                group=request.user.profile.students_group)
        result = {
            'id': a.id,
            'title': a.title,
            'text': a.text,
            'created_at': "" if a.created_at is None else a.created_at.strftime('%m/%d/%YT%H:%M'),
            'sended_at': "" if a.sended_at is None else a.sended_at.strftime('%m/%d/%YT%H:%M'),
        }
        return JsonResponse(result, safe=False)


class SendNewsView(View):
    def get(self, request, news_id):
        from curators.bots.vk_bot import VkBot
        news = get_object_or_404(News, pk=news_id)
        students = Profile.objects.filter(group=news.group)
        bot = VkBot(news.group)
        for student in students:
            bot.send_response_message({'user_id': student.vk_id,
                                       'text': f'{news.text}'})
        news.sended_at = now() + datetime.timedelta(hours=3)
        news.save()
        return JsonResponse({'status': 'OK'}, safe=False)


class StudentsView(View):
    def get(self, request):
        students = Profile.objects.filter(group=request.user.profile.students_group)
        result = [
            {
                'surname': c.surname,
                'first_name': c.first_name,
                'parent_name': '' if c.parent_name is None else c.parent_name,
                'vk': f'https://vk.com/id{c.vk_id}',
                'photo': '' if not c.vk_photo else c.vk_photo.url,
                'phone': c.phone,
                'birth_date': c.birth_date,
                'address': c.address,
            } for c in students
        ]
        return JsonResponse(result, safe=False)


class AchievementAskView(View):
    def get(self, request):
        asks = AchievementAsk.objects.filter(curator=request.user.profile, status__in=[1, 2])
        ach = Achievement.objects.filter(type=1).exclude(id__in=[x.achievement_id for x in asks])
        result = [
            {
                'id': a.id,
                'name': a.name,
            } for a in ach
        ]
        return JsonResponse(result, safe=False)

    def post(self, request):
        data = json.loads(request.body)
        ach_id = data['achievement_id']
        text = data['text']
        AchievementAsk.objects.create(curator=request.user.profile, achievement_id=ach_id, text=text)
        return JsonResponse({'status': 'OK'}, safe=False)


class RatingView(View):
    @staticmethod
    def get_rank(points):
        if points >= 1500:
            return 'Мега Дыбенко', 2200
        if points >= 1000:
            return 'Ультра Бач', 1500
        if points >= 500:
            return 'Маэстро Подгорный', 1000
        if points >= 200:
            return 'Адепт Бруевича', 500
        return 'Новичок', 200

    def get(self, request):
        ach = request.user.profile.achievements.all()
        sum = 0
        l = []
        for a in ach:
            sum += a.points
            l.append({'name': a.name, 'points': a.points})
        sum += 72 * request.user.profile.period_rating
        l.append({'name': "По итогам работы", 'points': 72 * request.user.profile.period_rating})
        sum += request.user.profile.seances_points
        l.append({'name': "Отзывы на кураторсткие", 'points': request.user.profile.seances_points})
        place = Profile.objects.values('opoints').annotate(id=Min('id')).filter(opoints__gt=sum).count()
        rank, next_points = self.get_rank(sum)
        result = {
            'points': sum,
            'next_rank_points': next_points,
            'rank': rank,
            'points_list': l,
            'place': place + 1
        }
        return JsonResponse(result, safe=False)


class RatingsView(View):
    def get(self, request):
        curators = Profile.objects.filter(role__in=['main_curator', 'curator'])
        place = curators.annotate(fpoints=Sum('achievements__points', field="achievements__points"))
        query = request.GET['query']
        if query:
            f = Q(first_name__icontains=query) | Q(surname__icontains=query) | \
                Q(students_group__number__icontains=query) | \
                Q(first_name__icontains=query.capitalize()) | Q(surname__icontains=query.capitalize()) | \
                Q(students_group__number__icontains=query.upper()) | Q(role=query) | Q(faculty__name=query)
            curators = curators.filter(f)
        for c in place:
            if c.fpoints is None:
                c.fpoints = 0
            c.opoints = c.fpoints + c.period_rating * 72 + c.seances_points
            c.save()
        place = Profile.objects.values('opoints').annotate(id=Min('id'))
        result = [{
            'id': c.id,
            'surname': c.surname,
            'first_name': c.first_name,
            'parent_name': '' if c.parent_name is None else c.parent_name,
            'points': c.opoints,
            'place': place.filter(opoints__gt=c.opoints).count() + 1,
            'group': '' if c.students_group is None else c.students_group.number,
            'faculty': '' if c.faculty is None else c.faculty.name,
            'role': c.get_verb_role(),
            'vk': f'https://vk.com/id{c.vk_id}',
            'photo': '' if not c.vk_photo else c.vk_photo.url,
        } for c in curators]
        result.sort(key=lambda x: x['place'])
        return JsonResponse(result, safe=False)


class Logout(View):
    def get(self, request):
        logout(request)
        return redirect('/')


class CuratorReviewView(View):
    def get(self, request, curator_id):
        c, _ = CuratorReview.objects.get_or_create(curator_id=curator_id)
        result = {
            'years': c.years,
            'curator_type': c.curator_type,
            'quality': c.quality,
            'attendance': c.attendance,
            'cooperation': c.cooperation,
            'informal': c.informal,
            'codex': c.codex,
            'personal_attitude': c.personal_attitude,
            'summary': c.summary,
            'points': c.points,  # int
            'reviewed': c.points is not None
        }
        return JsonResponse(result, safe=False)

    def post(self, request, curator_id):
        c = get_object_or_404(CuratorReview, curator_id=curator_id)
        data = json.loads(request.body)
        c.years = data['years']
        c.curator_type = data['curator_type']
        c.quality = data['quality']
        c.attendance = data['attendance']
        c.cooperation = data['cooperation']
        c.informal = data['informal']
        c.codex = data['codex']
        c.personal_attitude = data['personal_attitude']
        c.summary = data['summary']
        c.points = data['points']
        c.save()
        return JsonResponse({'status': 'OK'}, safe=False)


class CEventView(View):
    def get(self, request):
        events = Event.objects.all()
        result = [
            {
                'id': e.id,
                'title': e.title,
                'date_start': e.date_start.strftime('%d/%m/%Y'),
                'date_end': e.date_end.strftime('%d/%m/%Y'),
                'photo': None if e.photo.name == '' else e.photo.url
            } for e in events
        ]
        return JsonResponse(result, safe=False)


class EventReviewView(View):
    def get(self, request, event_id):
        profile = request.user.profile
        f = Review.objects.filter(seance__event_id=event_id, curator__faculty=profile.faculty)
        result = [
            {
                'id': a.id,
                'curator': {
                    'surname': a.curator.surname,
                    'first_name': a.curator.first_name,
                    'parent_name': '' if a.curator.parent_name is None else a.curator.parent_name,
                },
                'seance': {
                    'id': a.seance.id,
                    'title': a.seance.event.title,
                    'group': a.curator.students_group.number,
                    'time_start': a.seance.time_start if a.seance.time_start is None else a.seance.time_start.strftime(
                        '%m/%d/%YT%H:%M'),
                    'time_end': a.seance.time_end if a.seance.time_end is None else a.seance.time_end.strftime(
                        '%m/%d/%YT%H:%M'),
                },
                'how': a.how,
                'problems': a.problems,
                'what': a.what,
                'count': a.count,
                'created_at': a.created_at.strftime('%m/%d/%YT%H:%M'),
            } for a in f
        ]
        return JsonResponse(result, safe=False)


class MaterialsView(View):

    def get(self, request):
        materials = Material.objects.filter().order_by('-id')
        result = [
            {
                'id': h.id,
                'title': h.title,
                'text': h.text,
                'description': h.description,
                'attachment': None if h.attachments.name == '' else h.attachments.url,
                'photo': None if h.photo.name == '' else h.photo.url,
                'created_at': "" if h.created_at is None else (h.created_at + datetime.timedelta(hours=3)).strftime(
                    '%m/%d/%YT%H:%M'),
            } for h in materials
        ]
        return JsonResponse(result, safe=False)


class MaterialView(View):

    def get(self, request, material_id):
        h = get_object_or_404(Material, id=material_id)
        result = {
            'id': h.id,
            'title': h.title,
            'text': h.text,
            'description': h.description,
            'attachment': None if h.attachments.name == '' else h.attachments.url,
            'photo': None if h.photo.name == '' else h.photo.url,
            'created_at': "" if h.created_at is None else (h.created_at + datetime.timedelta(hours=3)).strftime(
                '%m/%d/%YT%H:%M'),
        }
        return JsonResponse(result, safe=False)

# from django.db.models import F
# from seances.models import Event,Seance,Feedback
# e = Event.objects.last()
# ss = Seance.objects.filter(event=e)
# for s in ss:
#     sum = 0
#     for f in Feedback.objects.filter(seance=s):
#         sum += f.points
#     st = s.group.student_profiles.count()
#     print(sum, st, s.group)
#     if st != 0:
#         for ccc in s.group.curator_profiles.all():
#             print(ccc, sum * 10 // st)
#         cur = s.group.curator_profiles.all().update(seances_points=F('seances_points') + sum * 10 // st)
