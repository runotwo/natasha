# Generated by Django 2.2.5 on 2020-12-24 15:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('seances', '0018_material_description'),
    ]

    operations = [
        migrations.AlterField(
            model_name='feedback',
            name='points',
            field=models.IntegerField(default=10),
        ),
    ]
