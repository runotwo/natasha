from django import forms
from django.contrib.admin.widgets import AdminSplitDateTime

from .models import Seance, Classroom


class SeanceForm(forms.ModelForm):

    class Meta:
        model = Seance
        fields = ('room', )