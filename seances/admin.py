from django.contrib import admin
from .models import Classroom, Event, Helper, Seance, Attendance, Review, Feedback, Material

# Register your models here.
admin.site.register(Classroom)
admin.site.register(Event)
admin.site.register(Helper)
admin.site.register(Attendance)
admin.site.register(Seance)
admin.site.register(Review)
admin.site.register(Feedback)
admin.site.register(Material)
