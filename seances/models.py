from django.db import models

from curators.models import Profile, Group


class Classroom(models.Model):
    title = models.CharField(max_length=255)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = 'Аудитории'


class Event(models.Model):
    class Types:
        CURATOR_HOUR = 1
        INFORMAL_CITY = 2
        INFORMAL_UNI = 3
        INFORMAL_GROUP = 4
        OTHER = 5

    type = models.CharField(max_length=255, verbose_name='Тип')
    title = models.CharField(max_length=255, verbose_name='Название')
    goal = models.TextField(max_length=255, verbose_name='Цель')
    date_start = models.DateField(verbose_name='Дата начала')
    date_end = models.DateField(verbose_name='Дата конца')
    photo = models.FileField(null=True, blank=True)

    def save(self, *args, **kwargs):
        new = self.pk is None
        super().save(*args, **kwargs)
        if new:
            group_ids = Profile.objects.filter(role__in=['curator', 'main_curator'],
                                               students_group__isnull=False).values_list('students_group',
                                                                                         flat=True).distinct()
            groups = Group.objects.filter(id__in=group_ids)
            for group in groups:
                Seance.objects.create(group=group, event=self)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = 'События'
        verbose_name = 'Событие'


class Helper(models.Model):
    title = models.CharField(max_length=255)
    text = models.TextField()
    attachments = models.FileField(null=True, blank=True)
    curator_hour = models.ForeignKey(Event, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return f'{self.title} ({self.curator_hour.title if self.curator_hour else "None"})'

    class Meta:
        verbose_name_plural = 'Памятки'


class Seance(models.Model):
    group = models.ForeignKey('curators.Group', on_delete=models.CASCADE, null=True, blank=True, verbose_name='Группа')
    event = models.ForeignKey(Event, on_delete=models.CASCADE, null=True, blank=True, verbose_name='Событие')
    time_start = models.DateTimeField(null=True, blank=True, verbose_name='Время начала')
    time_end = models.DateTimeField(null=True, blank=True, verbose_name='Время конца')
    room = models.ForeignKey(Classroom, on_delete=models.CASCADE, null=True, blank=True, verbose_name='Место')

    def __str__(self):
        return f'{self.event.title} ({self.group.faculty} {self.group.number})'

    class Meta:
        verbose_name_plural = 'Сеансы КЧ'

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        students = Profile.objects.filter(group=self.group)
        for s in students:
            Attendance.objects.get_or_create(seance=self, profile=s)


class Attendance(models.Model):
    seance = models.ForeignKey(Seance, on_delete=models.CASCADE)
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    would = models.BooleanField(default=False)
    here = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = 'Посещения'

    def __str__(self):
        return f'{self.seance} {self.profile}'


class Review(models.Model):
    seance = models.ForeignKey(Seance, on_delete=models.CASCADE, verbose_name='Сеанс')
    curator = models.ForeignKey('curators.Profile', null=True, blank=True, on_delete=models.SET_NULL,
                                verbose_name='Куратор')
    count = models.IntegerField(null=True, blank=True, verbose_name='Количество людей')
    how = models.TextField(null=True, blank=True, default='', verbose_name='Как прошел кураторский час?')
    problems = models.TextField(null=True, blank=True, default='',
                                verbose_name='Возникли ли проблемы, если да, как они решились?')
    what = models.TextField(null=True, blank=True, default='', verbose_name='Какие остались впечатления?')
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True, verbose_name='Время создания')

    def save(self, *args, **kwargs):
        self.count = Attendance.objects.filter(seance=self.seance, here=True).count()
        super().save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Обратная связь'
        verbose_name = 'Обратная связь'

    def __str__(self):
        return f'{self.seance} {self.curator}'


class Feedback(models.Model):
    student = models.ForeignKey(Profile, on_delete=models.CASCADE)
    seance = models.ForeignKey(Seance, on_delete=models.CASCADE)
    text = models.TextField(null=True)
    points = models.IntegerField(default=10)

    class Meta:
        verbose_name_plural = 'Отзывы'
        verbose_name = 'Отзыв'

    def __str__(self):
        return f'{self.seance} {self.student}'


class Material(models.Model):
    title = models.CharField(max_length=255)
    text = models.TextField()
    description = models.TextField(null=True, blank=True)
    attachments = models.FileField(null=True, blank=True)
    photo = models.FileField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True, verbose_name='Время создания')

    def __str__(self):
        return f'{self.title}'

    class Meta:
        verbose_name_plural = 'Материалы'
