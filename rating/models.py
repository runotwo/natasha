from django.db import models


class CodexParagraph(models.Model):
    name = models.CharField(max_length=512)
    description = models.TextField()
    weight = models.FloatField()

    class Meta:
        verbose_name_plural = 'Статьи кодекса'


class CodexCompliance(models.Model):
    curator = models.ForeignKey('curators.Profile', on_delete=models.CASCADE)
    codex_paragraph = models.ForeignKey(CodexParagraph, on_delete=models.CASCADE)
    value = models.IntegerField()

    class Meta:
        verbose_name_plural = 'Исполнение кодекса'


class Achievement(models.Model):
    TYPES = ((1, 'Ручное'),
             (2, 'Авто'))

    name = models.CharField(max_length=512)
    description = models.TextField()
    picture = models.FileField()
    points = models.IntegerField()
    type = models.IntegerField(choices=TYPES, default=2)

    class Meta:
        verbose_name_plural = 'Достижения'

    def __str__(self):
        return self.name

    def get_status(self, curator):
        if AchievementAsk.objects.filter(achievement=self, curator=curator,
                                         status=2).exists() or curator.achievements.filter(id=self.id).exists():
            return 'closed'
        if AchievementAsk.objects.filter(achievement=self, curator=curator, status=1).exists():
            return 'await'
        return 'available'


class AchievementAsk(models.Model):
    STATUSES = ((1, 'Новый'),
                (2, 'Выдано'),
                (3, 'Отклонен'))

    curator = models.ForeignKey('curators.Profile', on_delete=models.CASCADE)
    achievement = models.ForeignKey(Achievement, on_delete=models.CASCADE)
    text = models.TextField()
    status = models.IntegerField(choices=STATUSES, default=1)

    class Meta:
        verbose_name_plural = 'Запросы Достижений'

    def __str__(self):
        s = 'Новый'
        if self.status == 2:
            s = 'Выдано'
        if self.status == 3:
            s = 'Отклонен'

        return f'{self.curator} {self.achievement} ({s})'


class Task(models.Model):
    class Types:
        PREFATORY_APPEARANCE = 1
        APPEARANCE = 2
        HELPER_READING = 3
        PLACE_BOOKING = 4

    type = models.SmallIntegerField()
    curator = models.ForeignKey('curators.Profile', on_delete=models.CASCADE)
    event = models.ForeignKey('seances.Event', on_delete=models.CASCADE)
    done = models.BooleanField()

    class Meta:
        verbose_name_plural = 'Задачи'


class Division(models.Model):
    name = models.CharField(max_length=512)
    min_rating = models.IntegerField()
    achievements_for_get = models.ManyToManyField('Achievement')
    koef = models.FloatField()

    class Meta:
        verbose_name_plural = 'Дивизионы'

    def __str__(self):
        return self.name


class CuratorReview(models.Model):
    curator = models.ForeignKey('curators.Profile', on_delete=models.CASCADE, verbose_name='Сколько лет курирует')
    years = models.TextField(null=True, blank=True, verbose_name='Тип куратора')
    curator_type = models.TextField(null=True, blank=True, verbose_name='Качество проведения кураторских часов')
    quality = models.TextField(null=True, blank=True, verbose_name='Посещаемость собраний, отчетность')
    attendance = models.TextField(null=True, blank=True, verbose_name='Взаимодействие с первокурсниками')
    cooperation = models.TextField(null=True, blank=True, verbose_name='Неформальные встречи с группой')
    informal = models.TextField(null=True, blank=True, verbose_name='Общее поведение и соблюдение кодекса кураторов')
    codex = models.TextField(null=True, blank=True, verbose_name='Личный настрой куратора и впечатления от проделанной работы')
    personal_attitude = models.TextField(null=True, blank=True, verbose_name='Итоговый комментарий')
    summary = models.TextField(null=True, blank=True, verbose_name='Итоговая оценка')
    points = models.IntegerField(default=0, verbose_name='Баллы')
    approved = models.BooleanField(default=False, verbose_name='Подтверждено')

    def __str__(self):
        return str(self.curator)

    def save(self, *args, **kwargs):
        if self.approved:
            self.curator.period_rating = self.points
            self.curator.save()
        super().save(*args, **kwargs)
