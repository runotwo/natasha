from curators.models import Profile
from seances.models import Event
from .models import CodexCompliance, Task
from datetime import date
from .helpers import get_all_rivals_ch_points, get_curator_ch_points


def calculate_codex(profile: Profile):
    res = 1
    compliances = CodexCompliance.objects.filter(curator=profile)
    for compliance in compliances:
        res *= compliance.value ** compliance.codex_paragraph.weight
    return res


def calculate_achievements(profile: Profile):
    res = 0
    achievements = profile.achievements.all()
    for achievement in achievements:
        res += achievement.points
    return res


def calculate_work_plan(profile: Profile, period_start: date, period_end: date):
    all_tasks_count = Task.objects.filter(
        curator=profile,
        event__date_start__gte=period_start,
        event__date_start__lt=period_end).count()
    done_tasks_count = Task.objects.filter(
        curator=profile,
        event__date_start__gte=period_start,
        event__date_start__lt=period_end,
        done=True).count()
    return done_tasks_count / all_tasks_count * 100


def calculate_ch_quality(profile: Profile, period_start: date, period_end: date):
    rivals_points = get_all_rivals_ch_points(profile, period_start, period_end)
    curator_points = get_curator_ch_points(profile, period_start, period_end)
    result = 0
    for rival_points in rivals_points:
        if curator_points > rival_points:
            result += 1
        elif curator_points == rival_points:
            result += 0.5
    return result


def calculate_informal(profile: Profile, period_start: date, period_end: date):
    coefs = {
        Event.Types.INFORMAL_CITY: 1,
        Event.Types.INFORMAL_UNI: 0.7,
        Event.Types.INFORMAL_GROUP: 0.5,
    }

    result = 0
    seances = profile.seance_set.filter(
        event__date_start__gte=period_start,
        event__date_start__lt=period_end,
        event__type__in=[Event.Types.INFORMAL_GROUP, Event.Types.INFORMAL_CITY, Event.Types.INFORMAL_UNI]
    )
    for seance in seances:
        atd = seance.attendance_set.filter(here=True).count() / seance.attendance_set.filter().count() * 100
        result += atd * coefs[seance.event.type]
    return result


def calculate(profile: Profile, period_start: date, period_end: date, manual_rating):
    codex = calculate_codex(profile)
    achievements = calculate_achievements(profile)
    characteristic = calculate_work_plan(profile, period_start, period_end)
    ch_quality = calculate_ch_quality(profile, period_start, period_end)
    informal = calculate_informal(profile, period_start, period_end)
    manual = manual_rating
    return 0.29 * codex + 0.09 * achievements + 0.19 * characteristic + 0.14 * ch_quality + 0.05 * informal + 0.24 * manual


def calculate_elo(profile: Profile):
    rivals = Profile.objects.filter(division=profile.division)
    start_rating = profile.rating
    new_rating = float(profile.rating)
    for rival in rivals:
        Ea = 1 / (1 + 10 ** ((rival.rating - start_rating) / 400))
        pair_points = 0
        if profile.period_rating > rival.period_rating:
            pair_points = 1
        elif profile.period_rating == rival.period_rating:
            pair_points = 0.5
        new_rating += profile.division.koef * (pair_points - Ea)
    profile.rating = int(new_rating)
    return profile


def calculate_first_rating(profile: Profile):
    user_answers = profile.answerslog_set.filter(correct=True)
    points = 0
    for ans in user_answers:
        points += ans.question.points
    return points * profile.interview_points


def calculate_newhire_rating(profile: Profile, winner: Profile, n, m):
    return (100 * (profile.period_rating / winner.period_rating) * (2 * n - 2)) / (n + m - 2)
