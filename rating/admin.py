from django.contrib import admin
from .models import Achievement, AchievementAsk, CuratorReview

admin.site.register(Achievement)


@admin.register(AchievementAsk)
class AchievementAskAdmin(admin.ModelAdmin):
    change_form_template = 'ask_form.html'
    list_filter = ('status',)

    def response_change(self, request, ask: AchievementAsk):
        if "_yes" in request.POST:
            ask.status = 2
            ask.save()
            ask.curator.achievements.add(ask.achievement)
            ask.curator.save()
        if "_no" in request.POST:
            ask.status = 3
            ask.save()
        return super().response_change(request, ask)


@admin.register(CuratorReview)
class CuratorReviewAdmin(admin.ModelAdmin):
    list_display = ('curator', 'approved')
    list_filter = ('approved',)