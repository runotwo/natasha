class StateMachine(object):

    def __init__(self, default_state, states):
        self.default_state = default_state
        self.states = states

    def provide(self, *args, **kwargs):
        state = next((s for s in self.states if s.is_active(*args, **kwargs)), self.default_state)
        next_state = state.provide(*args, **kwargs)
        next_state.prepare(*args, **kwargs)
