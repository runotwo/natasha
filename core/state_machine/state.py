class State(object):

    def is_active(self, *args, **kwargs):
        raise NotImplementedError

    def prepare(self, *args, **kwargs):
        raise NotImplementedError

    def provide(self, *args, **kwargs):
        raise NotImplementedError
