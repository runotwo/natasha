from django.db import models


class Message(models.Model):
    user_from = models.ForeignKey('curators.Profile', on_delete=models.CASCADE, related_name='message_from')
    user_to = models.ForeignKey('curators.Profile', on_delete=models.CASCADE, related_name='message_to')
    created_at = models.DateTimeField(auto_now_add=True)
    text = models.TextField(default='')
    readed = models.BooleanField(default=False)
