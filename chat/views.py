from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.http import JsonResponse
from django.views import View

from .models import Message


class ChatsView(LoginRequiredMixin, View):
    login_url = '/admin/login/?next=/'

    def get(self, request):
        unreaded = request.query_params.get('unreaded') == 'true'
        messages = Message.objects.filter(user_to=request.user.profile) \
            .order_by('user_from', '-id') \
            .distinct('user_from')

        if unreaded:
            messages = messages.filter(readed=False)
        messages = messages.order_by('user_from', '-id') \
            .distinct('user_from')
        return JsonResponse([{
            'chat_id': m.user_from.id,
            'user_name': m.user_from.full_name,
            'message_text': m.text
        } for m in messages], safe=False)


class ChatView(LoginRequiredMixin, View):
    login_url = '/admin/login/?next=/'

    def get(self, request, chat_id):
        last_id = request.query_params.get('last_id')
        offset = request.query_params.get('offset')
        limit = request.query_params.get('limit')
        query = Q(user_from=request.user.profile) & Q(user_to_id=chat_id) | \
                Q(user_to=request.user.profile) & Q(user_from_id=chat_id)
        if last_id:
            last_id = int(last_id)
            query = query & Q(id__gt=last_id)
        result = Message.objects.filter(query).order_by('id')[offset:offset+limit]
        return JsonResponse([{
            'id': m.id,
            'user_from': m.user_from,
            'user_to': m.user_to,
            'created_at': m.created_at,
            'text': m.text,
        } for m in result], safe=False)

    def post(self, request, chat_id):
        message = Message(user_from=request.user.profile, user_to_id=chat_id, text=request.POST.get('text', ''))
        message.save()
        return JsonResponse({
            'id': message.id,
            'user_from': message.user_from,
            'user_to': message.user_to,
            'created_at': message.created_at,
            'text': message.text,
        })
