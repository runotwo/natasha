from django.urls import path

from .views import ChatView, ChatsView

urlpatterns = [
    path('chats/', ChatsView.as_view()),
    path('chats/<int:chat_id>/', ChatView.as_view()),
]
