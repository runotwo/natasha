from cwltool.factory import Factory, RuntimeContext


def main():
    runtime_context = RuntimeContext()
    runtime_context.outdir = '/tmp/test1'
    runtime_context.use_container = True

    factory = Factory(runtime_context=runtime_context)
    script_path = 'your_path/cat.cwl'

    cwl_script = factory.make(script_path)

    fd = open('your_path/hello.txt')
    res = cwl_script(message={
        'class': 'File',
        'contents': fd.read()
    })


if __name__ == '__main__':
    main()
