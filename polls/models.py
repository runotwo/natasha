from django.db import models


class Poll(models.Model):
    creator = models.ForeignKey('curators.Profile', on_delete=models.CASCADE)
    name = models.CharField(max_length=512)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'Опросы'


class Question(models.Model):
    poll = models.ForeignKey(Poll, on_delete=models.CASCADE)
    text = models.TextField(default='')

    class Meta:
        verbose_name_plural = 'Вопросы'


class Answer(models.Model):
    profile = models.ForeignKey('curators.Profile', on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    answer = models.TextField(default='')

    class Meta:
        verbose_name_plural = 'Ответы'


class News(models.Model):
    title = models.CharField(max_length=255)
    text = models.TextField()
    created_by = models.ForeignKey('curators.Profile', on_delete=models.CASCADE, null=True)
    group = models.ForeignKey('curators.Group', on_delete=models.CASCADE, null=True, blank=True, verbose_name='Группа')
    created_at = models.DateTimeField(null=True, auto_now_add=True)
    sended_at = models.DateTimeField(null=True)

    def __str__(self):
        return f'{self.group} | {self.text[:60]}'

    class Meta:
        verbose_name_plural = 'Новости'
