from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.views import View
from django.shortcuts import get_object_or_404
from seances.models import Profile
# from curators.bots.sender import Sender

from .models import Article, Poll, Question, Answer


class ArticleView(LoginRequiredMixin, View):
    login_url = '/admin/login/?next=/'

    def get(self, request, article_id):
        article = Article.objects.get(id=article_id)

        return JsonResponse({
            'id': article.id,
            'creator_name': article.creator.full_name,
            'creator_id': article.creator.id,
            'text': article.text,
            'created_at': article.created_at.isoformat(),
            'doc': article.doc if article.doc is None else article.doc.url
        })


class ArticlesView(LoginRequiredMixin, View):
    login_url = '/admin/login/?next=/'

    def get(self, request):
        articles = Article.objects.all().order_by('-created_at')

        return JsonResponse([{
            'id': a.id,
            'creator_id': a.creator.id,
            'creator_name': a.creator.full_name,
            'text': a.text,
            'doc': a.doc.url,
            'created_at': a.created_at.isoformat()
        } for a in articles])

    def post(self, request):
        article = Article.objects.create(
            creator=request.user.profile,
            text=request.POST.get('text', ''),
            need_send=request.POST.get('need_send') == 'true',
            doc=request.POST.get('doc', None)
        )
        return JsonResponse({
            'id': article.id,
            'creator_name': article.creator.full_name,
            'creator_id': article.creator.id,
            'text': article.text,
            'created_at': article.created_at.isoformat(),
            'doc': article.doc if article.doc is None else article.doc.url
        })


class SendArticleView(LoginRequiredMixin, View):
    login_url = '/admin/login/?next=/'

    def post(self, request, article_id):
        article = get_object_or_404(Article, pk=article_id)
        # students = Profile.objects.filter(group=request.user.profile.students_group)
        # for student in students:
        #     data['bot'].send_response_message({'user_id': student.vk_id, 'text': article.text, 'doc': article.doc})
        return JsonResponse({'result': 'OK'})


class PollsView(LoginRequiredMixin, View):
    login_url = '/admin/login/?next=/'

    def post(self, request):
        test = Poll.objects.create(
            creator=request.user.profile,
            name=request.POST.get('name', ''),
        )
        return JsonResponse({
            'id': test.id,
            'creator_name': test.creator.full_name,
            'creator_id': test.creator.id,
            'name': test.name,
        })

    def get(self, request):
        tests = Poll.objects.all().order_by('-created_at')
        return JsonResponse([{
            'id': test.id,
            'creator_name': test.creator.full_name,
            'creator_id': test.creator.id,
            'name': test.name,
        } for test in tests])

class QuestionsView(LoginRequiredMixin, View):
    login_url = '/admin/login/?next=/'

    def post(self, request, test_id):
        question = Question.objects.create(
            test_id=test_id,
            text=request.POST.get('text', ''),
        )
        return JsonResponse({
            'id': question.id,
            'poll_id': question.poll_id,
            'text': question.text,
        })

    def get(self, request, test_id):
        questions = Question.objects.filter(test_id=test_id)
        return JsonResponse([{
            'id': question.id,
            'poll_id': question.poll_id,
            'text': question.text,
        } for question in questions])

class SendPollView(LoginRequiredMixin, View):
    login_url = '/admin/login/?next=/'

    def post(self, request, poll_id):
        poll = get_object_or_404(Poll, pk=poll_id)
        students = Profile.objects.filter(group=request.user.profile.students_group)
        question = poll.question_set.first()
        for student in students:
            data['bot'].send_response_message({'user_id': student.vk_id, 'text': f'Пожалуйста, пройди опрос {poll.name}'})
            data['bot'].send_response_message({'user_id': student.vk_id, 'text': question.text})
            student.status = student.Status.POLL_ASKED
            student.asked_question = question
            student.save()
        return JsonResponse({'result': 'OK'})


class AnswersView(LoginRequiredMixin, View):
    login_url = '/admin/login/?next=/'

    def get(self, test_id, question_id):
        answers = Answer.objects.filter(question_id=question_id)
        return JsonResponse([{
            'id': answer.id,
            'text': answer.answer,
            'profile_id': answer.profile.id,
            'profile_name': answer.profile.full_name,
        } for answer in answers])


class QuestionView(LoginRequiredMixin, View):
    login_url = '/admin/login/?next=/'

    def patch(self, request, test_id, question_id):
        Question.objects.filter(id=question_id).update(**request.POST)
        return JsonResponse({'result': 'OK'})
