from django.apps import AppConfig


class TestsConfig(AppConfig):
    name = 'polls'
    verbose_name = 'Рассылки'
