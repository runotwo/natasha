from django.contrib import admin
from .models import Poll, Question, Answer, News

# Register your models here.
admin.site.register(Question)
admin.site.register(Answer)
admin.site.register(News)
admin.site.register(Poll)
