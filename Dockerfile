FROM python:3.6.8

WORKDIR /opt/app
RUN mkdir -p ./media

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . ./
CMD ["python3", "manage.py", "runserver", "0.0.0.0:8000"]
