
const routes = [
  {
    path: '/',
    component: () => import('layouts/LoginLayout.vue'),
    children: [
      { path: '', name: 'home', component: () => import('pages/Login.vue') }
    ]
  },
  {
    path: '/main',
    component: () => import('layouts/ProfileLayout.vue'),
    children: [
      { path: '', name: 'main', component: () => import('pages/Home.vue') }
    ]
  },
  {
    path: '/materials',
    component: () => import('layouts/ProfileLayout.vue'),
    children: [
      { path: '', name: 'materials', component: () => import('pages/Materials.vue') },
      { path: ':id', name: 'materials.item', component: () => import('pages/Material.vue') }
    ]
  },
  {
    path: '/profile',
    component: () => import('layouts/ProfileLayout.vue'),
    children: [
      { path: '', name: 'profile', component: () => import('pages/Profile.vue') }
    ]
  },
  {
    path: '/group',
    component: () => import('layouts/ProfileLayout.vue'),
    children: [
      { path: '', name: 'group', component: () => import('pages/Group.vue') }
    ]
  },
  {
    path: '/rating',
    component: () => import('layouts/ProfileLayout.vue'),
    children: [
      { path: '', name: 'rating', component: () => import('pages/Rating.vue') }
    ]
  },
  {
    path: '/control',
    component: () => import('layouts/ProfileLayout.vue'),
    children: [
      { path: '', name: 'control', component: () => import('pages/Control.vue') }
    ]
  },
  {
    path: '/events',
    component: () => import('layouts/ProfileLayout.vue'),
    children: [
      { path: '', name: 'events', component: () => import('pages/Events/FutureEvents.vue') },
      {
        path: 'future',
        name: 'events.future',
        component: () => import('pages/Events/FutureEvents.vue'),
        children: [
          { path: ':id', name: 'events.future.item', component: () => import('pages/Events/FutureEvent.vue') }
        ]
      },
      {
        path: 'past',
        name: 'events.past',
        component: () => import('pages/Events/PastEvents.vue'),
        children: [
          { path: ':id', name: 'events.past.item', component: () => import('pages/Events/PastEvent.vue') }
        ]
      }
    ]
  },
  {
    path: '/curators',
    component: () => import('layouts/ProfileLayout.vue'),
    children: [
      { path: '', name: 'curators', component: () => import('pages/Curators.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
