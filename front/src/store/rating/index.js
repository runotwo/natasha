import API from '../../api'

const state = {
  curators: []
}

const getters = {
  curators: state => state.curators
}

const mutations = {
  SET_CURATORS (state, payload) {
    state.curators = payload
  }
}
const actions = {
  loadRating ({ commit }) {
    return new Promise((resolve, reject) => {
      API.get('ratings?query=')
        .then(response => {
          commit('SET_CURATORS', response.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  searchRating ({ commit }, data) {
    return new Promise((resolve, reject) => {
      API.get('ratings?query=' + data.query)
        .then(response => {
          commit('SET_CURATORS', response.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
