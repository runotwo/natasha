import API from '../../api'

const state = {
  search_results: null,
  select_faculty: null,
  additional_curators: []
}

const getters = {
  search_results: state => state.search_results,
  select_faculty: state => state.select_faculty,
  additional_curators: state => state.additional_curators
}

const mutations = {
  LOAD_FACULTY_CURATORS (state, payload) {
    state.select_faculty = payload
  },
  LOAD_SEARCH_CURATORS (state, payload) {
    state.search_results = payload
  },
  LOAD_ADDITIONAL_CURATORS (state, payload) {
    state.additional_curators = payload
  }
}

const actions = {
  loadFaculty ({ commit }, data) {
    return new Promise((resolve, reject) => {
      API.get('curators?faculty=' + data.faculty)
        .then(response => {
          commit('LOAD_FACULTY_CURATORS', response.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  searchCurator ({ commit }, data) {
    return new Promise((resolve, reject) => {
      API.get('search_curators?query=' + data.search)
        .then(response => {
          commit('LOAD_SEARCH_CURATORS', response.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadAdditionalCurators ({ commit }) {
    return new Promise((resolve, reject) => {
      API.get('additional_curators')
        .then(response => {
          commit('LOAD_ADDITIONAL_CURATORS', response.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
