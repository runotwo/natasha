import API from '../../api'

const state = {
  future_events: null,
  past_events: null,
  select_event: null,
  rooms: [],
  attendance: null,
  reviews: null,
  materials: null
}

const getters = {
  future_events: state => state.future_events,
  past_events: state => state.past_events,
  select_event: state => state.select_event,
  rooms: state => state.rooms,
  attendance: state => state.attendance,
  reviews: state => state.reviews,
  materials: state => state.materials
}

const mutations = {
  LOAD_FUTURE_EVENTS (state, payload) {
    state.future_events = payload
  },
  LOAD_PAST_EVENTS (state, payload) {
    state.past_events = payload
  },
  SELECT_EVENT (state, payload) {
    state.select_event = payload
  },
  LOAD_ROOMS (state, payload) {
    state.rooms = payload
  },
  LOAD_ATTENDANCE (state, payload) {
    state.attendance = payload
  },
  LOAD_REVIEWS (state, payload) {
    state.reviews = payload
  },
  SELECT_MATERIALS (state, payload) {
    state.materials = payload
  }
}

const actions = {
  loadFutureEvents ({ commit }) {
    return new Promise((resolve, reject) => {
      API.get('seances/')
        .then(response => {
          commit('LOAD_FUTURE_EVENTS', response.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadPastEvents ({ commit }) {
    return new Promise((resolve, reject) => {
      API.get('ended_seances/')
        .then(response => {
          commit('LOAD_PAST_EVENTS', response.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadAttendance ({ commit }, data) {
    return new Promise((resolve, reject) => {
      API.get('seance/' + data.id + '/attendance/')
        .then(response => {
          commit('LOAD_ATTENDANCE', response.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  askAttendance ({ commit }, data) {
    return new Promise((resolve, reject) => {
      API.get('seance/' + data.id + '/ask_attendance/?news_id=' + data.news_id)
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  shareNewNews ({ commit }, data) {
    return new Promise((resolve, reject) => {
      API.get('seance/' + data.id + '/attendance_ask/' + data.news_id + '/')
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  createNews (_, data) {
    return new Promise((resolve, reject) => {
      API.post('news/', { text: data.text })
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  shareFeedback ({ commit }, data) {
    return new Promise((resolve, reject) => {
      API.get('seance/' + data.id + '/feedback_ask/')
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  attendanceEdit ({ commit }, data) {
    return new Promise((resolve, reject) => {
      API.post('seance/' + data.id + '/attendance/' + data.user + '/', { would: data.would, was: data.was })
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadFeedback ({ commit }, data) {
    return new Promise((resolve, reject) => {
      API.get('seance/' + data.id + '/feedback/')
        .then(response => {
          commit('LOAD_REVIEWS', response.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  eventInfo ({ commit }, data) {
    return new Promise((resolve, reject) => {
      API.get('seances/' + data.id + '/')
        .then(response => {
          commit('SELECT_EVENT', response.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadEventRooms ({ commit }, data) {
    return new Promise((resolve, reject) => {
      API.get('rooms/?time_start=' + data.start + '&time_end=' + data.end)
        .then(response => {
          commit('LOAD_ROOMS', response.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  reserveRoom ({ commit }, data) {
    return new Promise((resolve, reject) => {
      API.post('seances/' + data.id + '/reserve/', { time_start: data.start, time_end: data.end, room_id: data.room_id }, { headers: { 'Content-Type': 'application/json;charset=UTF-8', 'Accept-Language': 'en-Us' } })
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  clearSelectEvent ({ commit }) {
    commit('SELECT_EVENT', null)
    commit('LOAD_ATTENDANCE', null)
  },
  eventReview (_, data) {
    return new Promise((resolve, reject) => {
      API.post('seances/' + data.id + '/review/', { how: data.how, problems: data.problems, what: data.what })
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  cancelOrder ({ commit }, data) {
    return new Promise((resolve, reject) => {
      API.delete('seances/' + data.id + '/cancel/')
        .then(response => {
          commit('SELECT_EVENT', null)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadMaterials ({ commit }) {
    return new Promise((resolve, reject) => {
      API.get('materials/')
        .then(response => {
          commit('SELECT_MATERIALS', response.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadMaterialsInfo (_, data) {
    return new Promise((resolve, reject) => {
      API.get('materials/' + data.id + '/')
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
