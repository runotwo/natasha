import API from '../../api'

const state = {
  news: null,
  students: null
}

const getters = {
  news: state => state.news,
  students: state => state.students
}

const mutations = {
  LOAD_NEWS (state, payload) {
    state.news = payload
  },
  LOAD_STUDENTS (state, payload) {
    state.students = payload
  }
}

const actions = {
  loadNews ({ commit }) {
    return new Promise((resolve, reject) => {
      API.get('news/')
        .then(response => {
          commit('LOAD_NEWS', response.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  setNews ({ commit }, data) {
    return new Promise((resolve, reject) => {
      API.post('news/', { text: data.text })
        .then(response => {
          API.get('news/' + response.data.id + '/')
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadStudents ({ commit }) {
    return new Promise((resolve, reject) => {
      API.get('students/')
        .then(response => {
          commit('LOAD_STUDENTS', response.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
