import API from '../../api'

const state = {
  user: null,
  rating: null,
  achievements: null,
  achievements_asks: null
}

const getters = {
  user: state => state.user,
  rating: state => state.rating,
  achievements: state => state.achievements,
  achievements_asks: state => state.achievements_asks
}

const mutations = {
  SET_USER (state, payload) {
    state.user = payload
  },
  SET_RATING (state, payload) {
    state.rating = payload
  },
  SET_ACHIEVEMENTS (state, payload) {
    state.achievements = payload
  },
  SET_ACHIEVEMENTS_ASKS (state, payload) {
    state.achievements_asks = payload
  }
}
const actions = {
  loadUser ({ commit }) {
    return new Promise((resolve, reject) => {
      API.get('profile')
        .then(response => {
          commit('SET_USER', response.data)
          resolve(response)
        })
        .catch(error => {
          this.$router.push({ name: 'home' })
          reject(error)
        })
    })
  },
  loadRating ({ commit }) {
    return new Promise((resolve, reject) => {
      API.get('rating')
        .then(response => {
          commit('SET_RATING', response.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadAchievesAsks ({ commit }) {
    return new Promise((resolve, reject) => {
      API.get('achievement_asks')
        .then(response => {
          commit('SET_ACHIEVEMENTS_ASKS', response.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  sendAchieveRequest ({ commit }, data) {
    return new Promise((resolve, reject) => {
      API.post('achievement_asks', { achievement_id: data.id, text: data.text })
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadAchieves ({ commit }) {
    return new Promise((resolve, reject) => {
      API.get('achievements')
        .then(response => {
          commit('SET_ACHIEVEMENTS', response.data)
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
