import API from '../../api'

const state = {
}

const getters = {
}

const mutations = {
  SET_USER (state, payload) {
    state.user = payload
  }
}
const actions = {
  loadEvents ({ commit }) {
    return new Promise((resolve, reject) => {
      API.get('events/')
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  selectEvents ({ commit }, data) {
    return new Promise((resolve, reject) => {
      API.get('events/' + data.id + '/')
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  eventFeedback ({ commit }, data) {
    return new Promise((resolve, reject) => {
      API.get('seance/' + data.id + '/feedback/')
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadFaculty ({ commit }, data) {
    return new Promise((resolve, reject) => {
      API.get('curators?faculty=' + data.faculty)
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  sendReview ({ commit }, data) {
    return new Promise((resolve, reject) => {
      API.post('curators/' + data.id + '/review/', { years: data.years, curator_type: data.curator_type, quality: data.quality, attendance: data.attendance, cooperation: data.cooperation, informal: data.informal, codex: data.codex, personal_attitude: data.personal_attitude, summary: data.summary, points: data.points })
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  loadFeedback ({ commit }, data) {
    return new Promise((resolve, reject) => {
      API.get('curators/' + data.id + '/review/', { years: data.years, curator_type: data.curator_type, quality: data.quality, attendance: data.attendance, cooperation: data.cooperation, informal: data.informal, codex: data.codex, personal_attitude: data.personal_attitude, summary: data.summary, points: data.points })
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
