import axios from 'axios'

const baseURL = 'http://curator.sut.ru/api/'
// сюда api

const API = axios.create({
  baseURL: baseURL,
  headers: { 'Content-Type': 'application/json;charset=UTF-8', 'Access-Control-Allow-Origin': '*' }
})

API.defaults.headers.post['Content-Type'] = 'application/json;charset=UTF-8'

export default API
