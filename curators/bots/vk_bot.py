from django.http import HttpResponse
from vkmsg import VkClient
from vkmsg.models import messages

from core.state_machine import StateMachine
from .base_bot import BaseBot
from ..core_logic.states.address_state import AddressState
from ..core_logic.states.attendance_state import AttendanceState
from ..core_logic.states.await_name_state import NameState
from ..core_logic.states.birth_state import BirthState
from ..core_logic.states.block_state import BlockState
from ..core_logic.states.feedback_state import FeedbackState
from ..core_logic.states.feedback_points_state import FeedbackPointsState
from ..core_logic.states.phone_state import PhoneState
from ..core_logic.states.start_state import StartState
from ..models import Profile

state_machine = StateMachine(BlockState(),
                             [BlockState(), StartState(), AttendanceState(), FeedbackState(), AddressState(),
                              NameState(), BlockState(), BirthState(), PhoneState(), FeedbackPointsState()])


class VkBot(BaseBot):
    messenger_id = 'vk'

    def __init__(self, group):
        self.group = group
        self.confirm_string = group.confirmation_string
        self.bot = VkClient(group.bot_token, group.group_id)

    def text_message(self, incoming):
        return {'type': 'text', 'text': incoming.object.text, 'user_id': incoming.object.from_id, 'bot': self}

    def photo_message(self, incoming):
        photo = None
        photos = list(filter(lambda x: x.type == 'photo', incoming.object.attachments))
        if photos:
            photo = photos[0].photo.get_image()
        return {'type': 'photo', 'text': incoming.object.text, 'user_id': incoming.object.from_id, 'photo': photo,
                'bot': self}

    def incoming(self, message):
        data = message
        data['user'], created = Profile.objects.get_or_create(vk_id=data['user_id'])
        if created:
            data['user'].group = self.group
        state_machine.provide(data)
        return HttpResponse(self.get_response(data))

    def prepare_message(self, data):
        return self.bot.process_json(data)

    def send_response_message(self, data):
        message = messages.Message(data['text'])
        try:
            self.bot.send_message(int(data['user_id']), message)
        except Exception as e:
            msg = str(e) + ' ' + self.group.bot_token + ' ' + str(self.group.group_id)
            raise Exception(msg)

    def get_response(self, data):
        return self.confirm_string if data['type'] == 'confirmation' else 'ok'
