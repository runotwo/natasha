class Sender:
    @staticmethod
    def send(data):
        from . import VkBot

        VkBot().send_response_message(data)
