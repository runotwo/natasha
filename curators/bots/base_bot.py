import json
from abc import abstractmethod

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from core.state_machine import StateMachine
from ..core_logic.states.address_state import AddressState
from ..core_logic.states.attendance_state import AttendanceState
from ..core_logic.states.await_name_state import NameState
from ..core_logic.states.birth_state import BirthState
from ..core_logic.states.block_state import BlockState
from ..core_logic.states.feedback_state import FeedbackState
from ..core_logic.states.phone_state import PhoneState
from ..core_logic.states.start_state import StartState
from ..models import Profile

state_machine = StateMachine(BlockState(),
                             [StartState(), AttendanceState(), FeedbackState(), AddressState(), NameState(),
                              BlockState(), BirthState(), PhoneState()])


class BaseBot:
    message_types = ['text', 'photo']

    @staticmethod
    def get_response(data):
        return ''

    @classmethod
    def as_view(cls):
        return cls().incoming

    @property
    def messenger_id(self):
        raise NotImplementedError

    @csrf_exempt
    def incoming(self, request):
        data = json.loads(request.body)
        data = self.prepare_message(data)
        if data['type'] in self.message_types:
            data['user'], _ = Profile.objects.get_or_create(vk_id=data['user_id'])
            state_machine.provide(data)
        return HttpResponse(self.get_response(data))

    @abstractmethod
    def prepare_message(self, data):
        raise NotImplementedError

    @abstractmethod
    def send_response_message(self, data):
        raise NotImplementedError
