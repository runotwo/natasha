from django.apps import AppConfig
from django.db.models.signals import post_save


class CuratorsConfig(AppConfig):
    name = 'curators'
    verbose_name = 'Пользователи системы'

    def ready(self):
        from curators.signals import create_user_profile
