# Create your views here.

from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.views import View

from curators.models import Profile
from .forms import RegisterForm
from django.contrib.auth import authenticate, login


class CuratorsView(LoginRequiredMixin, View):
    login_url = '/admin/login/?next=/'

    def get(self, request, faculty_id):
        curators = Profile.objects.filter(group__faculty_id=faculty_id)
        return render(request, 'curators.html', context={'curators': curators})


class MainCuratorsView(LoginRequiredMixin, View):
    login_url = '/admin/login/?next=/'

    def get(self, request):
        return render(request, 'curators.html', context={
            'main_ik': Profile.objects.filter(role='main_ik').first(),
            'teach_curator': Profile.objects.filter(role='teach_curator',
                                                    students_group__faculty=request.user.profile.students_group.faculty).first(),
            'main_curator': Profile.objects.filter(role='main_curator',
                                                   students_group__faculty=request.user.profile.students_group.faculty).first(),
            'partner': Profile.objects.filter(role='main_ik',
                                              students_group=request.user.profile.students_group).exclude(
                id=request.user.profile.id).first(),
        })


def register(response):
    if response.method == "POST":
        form = RegisterForm(response.POST)
        if form.is_valid():
            form.save()
        return redirect("/home")
    else:
        form = RegisterForm()
    return render(response, "register.html", {"form": form})


def user_login(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
        return redirect('/')
    else:
        return redirect('http://127.0.0.1:8000/admin/login/?next=/admin/')
