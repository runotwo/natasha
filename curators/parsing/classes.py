from bs4 import BeautifulSoup
import requests
from ..models import Classes

PROGRAMS_URL = 'https://www.sut.ru/sveden/education'


def parse_classes():
    req = requests.get(PROGRAMS_URL)
    soup = BeautifulSoup(req.text, 'html.parser')
    prog_table = soup.find('th', text='Наличие практики (зачетные единицы – з.е.)').parent.parent
    program_iter = prog_table.children
    next(program_iter)
    next(program_iter)
    next(program_iter)
    next(program_iter)
    programs = next(program_iter)
    programs = programs.find_all('tr')
    for program in programs:
        tds = program.find_all('td')
        if tds[4].text != 'Очное':
            continue
        edu_code = tds[0].text
        edu_name = tds[1].text
        edu_profile = tds[3].text
        print(edu_code, edu_name, edu_profile, sep='|')
        prog_url = tds[5].find('a').attrs['href']
        classes_req = requests.get(prog_url)
        classes_soup = BeautifulSoup(classes_req.text, 'html.parser')
        classes_list = classes_soup.find_all('tr')[1:]
        for row in classes_list:
            if len(row) < 2:
                continue
            class_name = row.find_all('td')[0].text
            class_year = row.find_all('td')[1].text
            Classes.objects.get_or_create(
                edu_code=edu_code,
                edu_name=edu_name,
                edu_profile=edu_profile,
                year=class_year,
                name=class_name,
            )
