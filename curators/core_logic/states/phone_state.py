import re

from ...models import Profile
from core.state_machine import State
from .address_state import AddressState


class PhoneState(State):
    def prepare(self, data, *args, **kwargs):
        data['user'].status = Profile.Status.PHONE_ASKED
        data['user'].save()

    def is_active(self, data, *args, **kwargs):
        return data['user'].status == Profile.Status.PHONE_ASKED

    def provide(self, data, *args, **kwargs):
        response_data = {'user_id': data['user_id']}
        pattern = r'\+?[78]\D*(\d)\D*(\d)\D*(\d)\D*(\d)\D*(\d)\D*(\d)\D*(\d)\D*(\d)\D*(\d)\D*(\d)'
        phone = re.search(pattern, data['text'])
        if not phone:
            data['bot'].send_response_message({**response_data, **{'text': '''Пожалуйста, введи телефон в формате +79991112233'''}})
            return PhoneState()
        else:
            data['user'].phone = '+7' + ''.join(phone.groups()[:10])
        data['bot'].send_response_message({**response_data, **{'text': '''Последний шаг, укажи свой адрес и ближайшую станцию метро в формате:
ул. Пушкина 4 кв.111 м. Международная'''}})
        return AddressState()
