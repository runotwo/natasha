from core.state_machine import State
from seances.models import Attendance
from .block_state import BlockState
from ...models import Profile


class AttendanceState(State):
    def prepare(self, data, *args, **kwargs):
        data['user'].status = Profile.Status.ATTENDANCE_ASKED
        data['user'].save()

    def is_active(self, data, *args, **kwargs):
        return data['user'].status == Profile.Status.ATTENDANCE_ASKED

    def provide(self, data, *args, **kwargs):
        response_data = {'user_id': data['user_id']}
        answers = [('Увидимся на кураторском', True), ('Если передумаешь, буду ждать', False)]
        try:
            ans = answers[int(data.get('text')) - 1]
        except:
            data['bot'].send_response_message({**response_data, **{'text': 'Будет классно, если ты напишешь 1 или 2.'}})
            return AttendanceState()
        Attendance.objects.filter(seance=data['user'].asked_seance,
                                  profile=data['user']).update(would=ans[1])
        data['user'].asked_seance = None
        data['user'].save()
        data['bot'].send_response_message({**response_data, **{'text': ans[0]}})
        return BlockState(force=True)
