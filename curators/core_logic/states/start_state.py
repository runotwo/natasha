from ...models import Profile
from core.state_machine import State
from .await_name_state import NameState


class StartState(State):
    def __init__(self, force=False):
        self.force = force

    def prepare(self, data, *args, **kwargs):
        data['user'].status = Profile.Status.STARTED if self.force else data['user'].status
        data['user'].save()

    def is_active(self, data, *args, **kwargs):
        return data['user'].status == Profile.Status.STARTED

    def provide(self, data, *args, **kwargs):
        response_data = {'user_id': data['user_id']}
        data['bot'].send_response_message({**response_data, **{'text': '''Привет! Это твой бот-куратор
Он будет оповещать тебя об основных новостях и событиях

Пожалуйста, для заполнения контактных данных напиши свое полное имя в формате:
Иванов Иван Иванович'''}})
        return NameState()
