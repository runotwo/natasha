from core.state_machine import State
from seances.models import Feedback
from .block_state import BlockState
from ...models import Profile


class FeedbackPointsState(State):
    def prepare(self, data, *args, **kwargs):
        data['user'].status = Profile.Status.FEEDBACK_POINTS_ASKED
        data['user'].save()

    def is_active(self, data, *args, **kwargs):
        return data['user'].status == Profile.Status.FEEDBACK_POINTS_ASKED

    def provide(self, data, *args, **kwargs):
        response_data = {'user_id': data['user_id'], 'text': 'Спасибо за обратную связь'}
        f = Feedback.objects.filter(seance=data['user'].asked_seance, student=data['user']).last()
        points = -1
        try:
            points = int(data['text'])
        except:
            pass
        if not (1 <= points <= 10):
            response_data = {'user_id': data['user_id'], 'text': 'Поставь оценку от 1 до 10'}
            data['bot'].send_response_message({**response_data})
            return FeedbackPointsState()
        f.points = points
        f.save()
        data['user'].asked_seance = None
        data['user'].save()
        data['bot'].send_response_message({**response_data})
        return BlockState(force=True)
