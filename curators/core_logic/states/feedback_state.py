from core.state_machine import State
from seances.models import Feedback
from .block_state import BlockState
from .feedback_points_state import FeedbackPointsState
from ...models import Profile


class FeedbackState(State):
    def prepare(self, data, *args, **kwargs):
        data['user'].status = Profile.Status.FEEDBACK_ASKED
        data['user'].save()

    def is_active(self, data, *args, **kwargs):
        return data['user'].status == Profile.Status.FEEDBACK_ASKED

    def provide(self, data, *args, **kwargs):
        response_data = {'user_id': data['user_id'], 'text': 'Поставь оценку от 1 до 10'}
        Feedback.objects.create(seance=data['user'].asked_seance, student=data['user'], text=data.get('text'))
        data['bot'].send_response_message({**response_data})
        return FeedbackPointsState()
