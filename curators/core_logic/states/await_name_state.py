from ...models import Profile
from core.state_machine import State
from .birth_state import BirthState


class NameState(State):
    def prepare(self, data, *args, **kwargs):
        data['user'].status = Profile.Status.NAME_ASKED
        data['user'].save()

    def is_active(self, data, *args, **kwargs):
        return data['user'].status == Profile.Status.NAME_ASKED

    def provide(self, data, *args, **kwargs):
        response_data = {'user_id': data['user_id']}
        parts = data['text'].split()
        if len(parts) < 2:
            data['bot'].send_response_message({**response_data, **{'text': '''Пожалуйста, для заполнения контактных данных напиши свое полное имя в формате:
Иванов Иван Иванович'''}})
            return NameState()
        data['user'].surname = parts[0]
        data['user'].first_name = parts[1]
        if len(parts) == 3:
            data['user'].parent_name = parts[2]
        data['bot'].send_response_message({**response_data, **{'text': '''Спасибо, а теперь напиши свою дату рождения в формате:
01.01.2000'''}})
        return BirthState()
