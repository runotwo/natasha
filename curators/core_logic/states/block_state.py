from ...models import Profile
from core.state_machine import State
from .await_name_state import NameState


class BlockState(State):
    def __init__(self, force=False):
        self.force = force

    def prepare(self, data, *args, **kwargs):
        data['user'].status = Profile.Status.BLOCK if self.force else data['user'].status
        data['user'].save()

    def is_active(self, data, *args, **kwargs):
        return data['user'].status == Profile.Status.BLOCK or data['user'].role in ['curator', 'main_curator']

    def provide(self, data, *args, **kwargs):
        return BlockState()
