import datetime
import re

from ...models import Profile
from core.state_machine import State
from .phone_state import PhoneState


class BirthState(State):
    def prepare(self, data, *args, **kwargs):
        data['user'].status = Profile.Status.BIRTH_ASKED
        data['user'].save()

    def is_active(self, data, *args, **kwargs):
        return data['user'].status == Profile.Status.BIRTH_ASKED

    def provide(self, data, *args, **kwargs):
        response_data = {'user_id': data['user_id']}
        birth = re.search(r"\d\d\.\d\d\.\d\d\d\d", data['text'])
        if not birth:
            data['bot'].send_response_message({**response_data, **{'text': 'Пожалуйста, введи дату рождения в формате 01.01.2000'}})
            return BirthState()
        else:
            birth = birth.group(0)
            try:
                datetime.datetime.strptime(birth, '%d.%m.%Y')
            except:
                data['bot'].send_response_message({**response_data, **{'text': 'Пожалуйста, введи дату рождения в формате 01.01.2000'}})
                return BirthState()
            data['user'].birth_date = birth
        data['bot'].send_response_message({**response_data, **{'text': '''Также понадобится твой номер телефона, укажи его в формате:
+79991112233'''}})
        return PhoneState()
