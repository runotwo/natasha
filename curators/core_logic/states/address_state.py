from ...models import Profile
from core.state_machine import State


class AddressState(State):
    def prepare(self, data, *args, **kwargs):
        data['user'].status = Profile.Status.ADDRESS_ASKED
        data['user'].save()

    def is_active(self, data, *args, **kwargs):
        return data['user'].status == Profile.Status.ADDRESS_ASKED

    def provide(self, data, *args, **kwargs):
        from .block_state import BlockState
        response_data = {'user_id': data['user_id']}
        data['user'].address = data['text']
        data['bot'].send_response_message({**response_data, **{'text': '''Увидимся на кураторском!'''}})
        return BlockState(force=True)
