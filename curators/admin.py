from django.contrib import admin

from .models import Profile, Group, Faculty

# Register your models here.
admin.site.register(Group)
admin.site.register(Faculty)


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    search_fields = ['surname', 'first_name', 'parent_name']
