from django.contrib.auth.models import User
from .models import Profile
from django.db.models.signals import post_save
from django.dispatch import receiver

print('HERE')

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    vk = instance.social_auth.first()
    if vk:
        profile = Profile.objects.filter(vk_id=vk.uid).first()
        if profile:
            profile.user = instance
            profile.save()
