import json

from django.http import HttpResponse
from vkmsg.models.requests import IncomingRequest


class BotFarm:
    bots = {}

    def init_bots(self):
        from curators.models import Group
        from curators.bots.vk_bot import VkBot
        groups = Group.objects.all()
        for group in groups:
            if group.bot_token is not None:
                self.bots[group.group_id] = VkBot(group)

    @classmethod
    def as_view(cls):
        return cls().incoming

    def incoming(self, request):
        try:
            msg = json.loads(request.body)
        except:
            return HttpResponse("ok")
        request = IncomingRequest(**msg)
        if self.bots.get(request.group_id) is None:
            self.init_bots()
        bot = self.bots.get(request.group_id)

        if bot is None:
            return HttpResponse("ok")
        if request.type == 'confirmation':
            return HttpResponse(bot.confirm_string)
        try:
            message = bot.text_message(request)
        except:
            return HttpResponse("ok")
        return bot.incoming(message)
