# Generated by Django 2.2.5 on 2020-12-24 16:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('curators', '0023_profile_seances_points'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='study_rating',
        ),
        migrations.AddField(
            model_name='profile',
            name='opoints',
            field=models.IntegerField(default=0),
        ),
    ]
