# Generated by Django 2.2.5 on 2020-10-29 10:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('curators', '0016_auto_20201012_2344'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='address',
            field=models.CharField(blank=True, max_length=1024, null=True, verbose_name='День рождения'),
        ),
        migrations.AddField(
            model_name='profile',
            name='birth_date',
            field=models.CharField(blank=True, max_length=512, null=True, verbose_name='День рождения'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='status',
            field=models.IntegerField(blank=True, default=0, null=True, verbose_name='Статус (бот)'),
        ),
    ]
