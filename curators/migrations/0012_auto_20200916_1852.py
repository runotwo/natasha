# Generated by Django 2.2.5 on 2020-09-16 15:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('curators', '0011_auto_20200831_1428'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='full_name',
        ),
        migrations.AddField(
            model_name='profile',
            name='first_name',
            field=models.CharField(blank=True, max_length=512, null=True, verbose_name='Имя'),
        ),
        migrations.AddField(
            model_name='profile',
            name='parent_name',
            field=models.CharField(blank=True, max_length=512, null=True, verbose_name='Отчество'),
        ),
        migrations.AddField(
            model_name='profile',
            name='surname',
            field=models.CharField(blank=True, max_length=512, null=True, verbose_name='Фамилия'),
        ),
    ]
