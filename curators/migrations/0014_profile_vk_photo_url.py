# Generated by Django 2.2.5 on 2020-09-26 16:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('curators', '0013_profile_faculty'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='vk_photo_url',
            field=models.CharField(blank=True, max_length=512, null=True, verbose_name='Url фото ВКонтакте'),
        ),
    ]
