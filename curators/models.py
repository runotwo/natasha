from django.core.files.base import ContentFile
from django.contrib.auth.models import User
from django.db import models
import requests


class Faculty(models.Model):
    name = models.CharField(max_length=63)
    full_name = models.CharField(max_length=255, default='')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Факультеты'


class Group(models.Model):
    faculty = models.ForeignKey(Faculty, on_delete=models.SET_NULL, null=True, blank=True)
    number = models.CharField(max_length=63)
    course = models.IntegerField()
    bot_token = models.CharField(max_length=256, null=True, blank=True)
    group_id = models.IntegerField(null=True, blank=True)
    confirmation_string = models.CharField(max_length=63, null=True, blank=True)

    def __str__(self):
        return f'{self.number} ({self.faculty.name if self.faculty else ""})'

    class Meta:
        verbose_name_plural = 'Группы'


class Profile(models.Model):
    ROLES = (('admin', 'Администратор'),
             ('teach_curator', 'Педагог-куратор'),
             ('main_curator', 'Старший куратор'),
             ('curator', 'Куратор'),
             ('candidate', 'Кандидат в кураторы'),
             ('student', 'Студент'))

    class Status:
        STARTED = 0
        ATTENDANCE_ASKED = 1
        FEEDBACK_ASKED = 2
        POLL_ASKED = 3
        NAME_ASKED = 4
        BIRTH_ASKED = 5
        PHONE_ASKED = 6
        ADDRESS_ASKED = 7
        BLOCK = 8
        FEEDBACK_POINTS_ASKED = 9

    surname = models.CharField(max_length=512, verbose_name='Фамилия', null=True, blank=True)
    first_name = models.CharField(max_length=512, verbose_name='Имя', null=True, blank=True)
    parent_name = models.CharField(max_length=512, verbose_name='Отчество', null=True, blank=True)
    profile_image = models.FileField(null=True, blank=True, verbose_name='Фото профиля')
    role = models.CharField(choices=ROLES, max_length=80, default='student', verbose_name='Тип', null=True, blank=True)
    vk_id = models.CharField(max_length=512, verbose_name='Id ВКонтакте', null=True, blank=True)
    vk_photo = models.FileField(verbose_name='Фото ВКонтакте', null=True, blank=True)
    phone = models.CharField(max_length=512, verbose_name='Номер телефона', null=True, blank=True)
    birth_date = models.CharField(max_length=512, verbose_name='День рождения', null=True, blank=True)
    address = models.CharField(max_length=1024, verbose_name='Адрес проживания', null=True, blank=True, default=Status.STARTED)
    status = models.IntegerField(verbose_name='Статус (бот)', default=0, null=True, blank=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE, blank=True, null=True, verbose_name='Django пользователь')
    group = models.ForeignKey(Group, on_delete=models.SET_NULL, blank=True, null=True, related_name='student_profiles', verbose_name='Группа')
    students_group = models.ForeignKey(Group, on_delete=models.SET_NULL, blank=True, null=True, related_name='curator_profiles', verbose_name='Курируемая группа')
    faculty = models.ForeignKey(Faculty, on_delete=models.SET_NULL, blank=True, null=True, verbose_name='Курируемый факультет')
    asked_seance = models.ForeignKey('seances.Seance', on_delete=models.SET_NULL, blank=True, null=True,
                                     related_name='atd_ask', verbose_name='Сеанс (бот)')
    asked_question = models.ForeignKey('polls.Question', on_delete=models.SET_NULL, blank=True, null=True,
                                       related_name='q_ask', verbose_name='Вопрос (бот)')
    achievements = models.ManyToManyField('rating.Achievement', verbose_name='Достижения', null=True, blank=True)
    division = models.ForeignKey('rating.Division', on_delete=models.SET_NULL, null=True, verbose_name='Дивизион', blank=True)
    rating = models.IntegerField(verbose_name='Основной рейтинг', null=True, blank=True)
    opoints = models.IntegerField(default=0)
    period_rating = models.IntegerField(verbose_name='Рейтинг в периоде', default=0)
    seances_points = models.IntegerField(verbose_name='Баллы за кураторские', default=0)
    interview_points = models.FloatField(verbose_name='Баллы за собеседование', null=True, blank=True)
    tested = models.BooleanField(verbose_name='Тестирование пройдено', default=False)

    def __str__(self):
        return f'{self.surname} {self.first_name}'

    def get_verb_role(self):
        roles = dict(self.ROLES)
        return roles.get(self.role, '')

    class Meta:
        verbose_name_plural = 'Профили'

    def save(self, *args, **kwargs):
        if not self.vk_photo:
            resp = requests.get(f'https://api.vk.com/method/users.get?user_ids={self.vk_id}&fields=photo_400_orig&name_case=Nom&access_token=b0e093a2b0e093a2b0e093a295b0932523bb0e0b0e093a2efadb438ee3f3ae7f4fce7e7&v=5.103')
            data = resp.json()
            vk_photo_url = data['response'][0]['photo_400_orig']
            resp = requests.get(vk_photo_url)
            photo = ContentFile(resp.content)
            photo.name = f'{self.vk_id}.jpg'
            self.vk_photo = photo
        super().save(*args, **kwargs)


class Journal(models.Model):
    teacher = models.OneToOneField(Profile, on_delete=models.CASCADE)
    event_participations = models.TextField()
    social_participations = models.TextField()
    missions = models.TextField()
    characteristic = models.TextField()
    difficult_students = models.TextField()
    active = models.TextField()
    social_students = models.TextField()
    main_events = models.TextField()
    year_reports = models.TextField()


class WorkPlan(models.Model):
    teacher = models.OneToOneField(Profile, on_delete=models.CASCADE)
    name = models.CharField(max_length=512)
    description = models.TextField()
    goals = models.TextField()
    time_end = models.DateTimeField(null=True, blank=True)


class ScheduleLesson(models.Model):
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    day = models.IntegerField()
    week = models.IntegerField()
    pair = models.IntegerField()
    name = models.CharField(max_length=100)
    type = models.CharField(max_length=100)
    place = models.CharField(max_length=100)
    teacher = models.CharField(max_length=100)
